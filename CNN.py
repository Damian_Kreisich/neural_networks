import pickle as cPickle
import gzip
import time
import numpy as np
import backpropagation as bp
from matplotlib import pyplot as plt


def load_data(file_name):
    f = gzip.open(file_name, 'rb')
    training_data, validation_data, test_data = cPickle.load(f, encoding='latin1')
    f.close()
    return training_data, validation_data, test_data


def prepare_random_data_cnn(size, min, max):
    window = np.random.uniform(low=min, high=max, size=size)
    # bias = np.zeros(layers)
    return window


def create_reshape(matrix):
    sqrt = int(np.sqrt(matrix.shape[1]))
    return matrix.reshape(matrix.shape[0], sqrt, sqrt, 1)


def convolve(window, dataset, step):
    filter_size = window.shape[0]
    result_img_size = dataset.shape[1] - filter_size + 1
    affected_matrixes = np.zeros(
        (dataset.shape[0], result_img_size, result_img_size, dataset.shape[3] * filter_size ** 2))

    for i in range(0, filter_size, step):
        for j in range(0, filter_size, step):
            affected_matrixes[:, :, :,
            (i * filter_size + j) * dataset.shape[3]:((i * filter_size + j) * dataset.shape[3]) + dataset.shape[3]] = \
                dataset[:, i:i + result_img_size, j:j + result_img_size, :]

    result = affected_matrixes @ window.reshape(filter_size ** 2 * window.shape[2], window.shape[3])

    return result


def initialize():
    hidden_layer = 50
    weights_range = [(-0.25, 0.25)]

    first_window = prepare_random_data_cnn((window_size, window_size, channels, filters), -5, 5)
    simple_window = prepare_random_data_cnn((1, 1, filters, 1), -5, 5)

    hidden_weights, hidden_biases = bp.prepare_random_data(np.zeros(shape=(50000, 676)), hidden_layer,
                                                           weights_range[0][0], weights_range[0][1])
    result_weights, result_biases = bp.prepare_random_data(hidden_weights, 10, weights_range[0][0], weights_range[0][1])
    weights = [hidden_weights, result_weights]
    biases = [hidden_biases, result_biases]
    return first_window, simple_window, weights, biases


def forward(training, first_window, simple_window, wgh, bs, batch_size):
    impulses = []
    activations = []
    firstimp = convolve(first_window, training, 1)
    impulses.append(firstimp)
    firstacc = bp.get_activation_function("relu")(firstimp)
    activations.append(firstacc)
    afterSplashimp = convolve(simple_window, firstacc, 1)
    impulses.append(afterSplashimp)
    afterSplash = bp.get_activation_function("relu")(afterSplashimp).reshape(batch_size, afterSplashimp.shape[1]**2)
    activations.append(afterSplash)

    activations_final, impulses_final = bp.forward_pass(afterSplash, wgh, bs)

    return impulses, activations, impulses_final, activations_final


def test_validate(labels, data, fwin, swin, wgh, bs, batch_size):
    _, _, z, a = forward(data, fwin, swin, wgh, bs, batch_size)
    pred = np.argmax(a[-1], axis=1)
    return (np.sum(pred == labels)/len(data))


def train(training, labels, validation, validation_labels, batch_size=100, lr=0.001, epochs=50):
    table = []
    fwin, swin, wgh, bs = initialize()

    last_change_weights = []
    last_change_biases = []

    m = []
    v = []
    mbias = []
    vbias = []

    for w in wgh:
        last_change_weights.append(np.ones(w.shape))
        m.append(np.ones(w.shape))
        v.append(np.ones(w.shape))
    for b in bs:
        last_change_biases.append(np.ones(b.shape))
        mbias.append(np.ones(b.shape))
        vbias.append(np.ones(b.shape))

    for i in range(epochs):
        for i in range(0, training.shape[0], batch_size):
            x_batch = training[i:i + batch_size]
            y_batch = labels[i:i + batch_size]
            imp_con, act_con, imp_fin, act_fin = forward(x_batch, fwin, swin, wgh, bs, batch_size)
            dw, db, d = bp.back_propagation(wgh, y_batch, act_fin, imp_fin, batch_size)

            # wgh = [w + lr * dweight.T for w, dweight in zip(wgh, dw)]
            # bs = [w + lr * dbias.T for w, dbias in zip(bs, db)]
            iteratemom = 0
            for w, dweight in zip(wgh, dw):
                temp_weights = 0.9 * last_change_weights[iteratemom] + 0.1 * dweight.T ** 2
                wgh[iteratemom] = w + (last_change_weights[iteratemom] / temp_weights) * dweight.T
                last_change_weights[iteratemom] = temp_weights
                iteratemom = iteratemom + 1

            iteratemom = 0
            for w, dbias in zip(bs, db):
                temp_biases = 0.9 * last_change_biases[iteratemom] + 0.1 * dbias.T ** 2
                bs[iteratemom] = w + (last_change_biases[iteratemom] / temp_biases) * dbias.T
                last_change_biases[iteratemom] = temp_biases
                iteratemom = iteratemom + 1


            delta_3 = wgh[0].dot(d[0]).T * \
                      bp.get_derivative_activation_function('relu')(imp_con[1].reshape(batch_size, imp_con[1].shape[1]**2))
            delta_3 = create_reshape(delta_3)
            delta_4 = convolve(np.rot90(np.rot90(np.reshape(swin,
                                                            newshape=(
                                                                swin.shape[0], swin.shape[1], swin.shape[3],
                                                                swin.shape[2])))),
                               delta_3, 1) * bp.get_derivative_activation_function('relu')(imp_con[0])

            swin = update_window(delta_3, swin, act_con[0], lr)
            fwin = update_window(add_padding(delta_4, window_size-1), fwin, x_batch, lr)
            #
            # single_val = test_validate(validation_labels, validation, fwin, swin, wgh, bs, validation_labels.shape[0])
            # table.append(single_val)
            # print("Wynik dla walidującego ciągu {}".format(single_val))


        single_val = test_validate(validation_labels, validation, fwin, swin, wgh, bs, validation_labels.shape[0])
        table.append(single_val)
        print("Wynik dla walidującego ciągu {}".format(single_val))

    return test_validate(test_labels, test_input, fwin, swin, wgh, bs, test_labels.shape[0]), table


def add_padding(image, padding):
    res = np.pad(image, pad_width=[(0, 0), (padding, padding), (padding, padding), (0, 0)], constant_values=0)

    # result_array = np.zeros(
    #     (image.shape[0], image.shape[1] + 2 * padding, image.shape[2] + 2 * padding, image.shape[3]))
    # result_array[:, padding:-padding, padding:-padding, :] = image
    return res


def update_window(delta, window, activation, lr):
    rotated_activation = np.rot90(np.rot90(activation))
    update = convolve(np.reshape(rotated_activation,
                                 newshape=(
                                     rotated_activation.shape[1], rotated_activation.shape[2],
                                     rotated_activation.shape[0],
                                     rotated_activation.shape[3])
                                 ),
                      np.reshape(delta,
                                 newshape=(
                                     delta.shape[3], delta.shape[1], delta.shape[2], delta.shape[0]))
                      , 1)
    return window + lr * np.reshape(update, newshape=window.shape)




training_data, validation_data, test_data = load_data("mnist.pkl.gz")
training_input = create_reshape(training_data[0])
training_labels = bp.one_hot(training_data[1], 10)


window_size = 3
convolve_result_size = 676
channels = 1
filters = 8

validation_input = create_reshape(validation_data[0])
validation_labels = validation_data[1]

test_input = create_reshape(test_data[0])
test_labels = test_data[1]

epochs = 3
reps = 1

with open('tests.csv', 'w') as csv:
    csv.write(
        "{}|{}|{}|{}|{}|{}|{}|{}\n".format("Learning rate", "MinMaxes", "Batch size", "Hidden layer size", "Epochs",
                                           "Reps for avg", "Final result", "Time in seconds"))


timerStart = time.time()

testResults, final_table = train(training_input, training_labels.T, validation_input, validation_labels.T, epochs=epochs)

timerStop = time.time()


title = "CNN 3x3"
bp.create_plot(epochs, np.array(final_table)/reps, title)

with open('tests.csv', 'a') as csv:
    csv.write(
        "{}|{}|{}|{}|{}|{}|{}|{}\n".format(0.001, "(-5,5)", 100, 50, epochs, reps,
                                           np.sum(testResults)/reps, timerStop - timerStart))

print("Finished")
