import numpy as np
from matplotlib import pyplot

#OrData
or_dataset1 = (1, -1, -1)
or_dataset2 = (1, -1, 1)
or_dataset3 = (1, 1, -1)
or_dataset4 = (1, 1, 1)

set = np.array([or_dataset1, or_dataset2, or_dataset3, or_dataset4])


# mode = 0 - unipolar p|1 - bipolar / function = 0 - or 1 - and
def prepare_data(mode, function, min, max, bias):
    dataset_array = []
    if (bias == 0):
        dataset_array.append((0, 0))
        dataset_array.append((0, 1))
        dataset_array.append((1, 0))
        dataset_array.append((1, 1))
    elif (bias == 1):
        dataset_array.append((1, 0, 0))
        dataset_array.append((1, 0, 1))
        dataset_array.append((1, 1, 0))
        dataset_array.append((1, 1, 1))

    data_array = []
    if (function == 1):
        data_array.append(0)
        data_array.append(0)
        data_array.append(0)
        data_array.append(1)
    else:
        data_array.append(0)
        data_array.append(1)
        data_array.append(1)
        data_array.append(1)

    dataset = np.array(dataset_array)
    result = np.array(data_array)
    if (mode == 1):
        result = (result * 2) - 1
        dataset = (dataset * 2) - 1

    if bias == 1:
        weights = np.random.uniform(min, max, (1, 3))
    elif bias == 0:
        weights = np.random.uniform(min, max, (1, 2))

    return result, weights, dataset


def create_adaline(set, w, res, learning_rate, stop_error):
    actual_error = 9999
    i = 0

    while stop_error < actual_error:
        pred = predictions(set, w)
        e = count_e(pred, res)
        actual_error = avg_ek2(e, set)
        # print(sum)
        w = w + 2 * learning_rate * e[i % 4] * set[i % 4]
        # print(w)
        i+=1
    return w, i


def predictions(x, w):
    pred = np.zeros(shape=4)
    for i in range(len(x)):
        pred[i] = count_y(w, x[i])
    return pred


def count_y(w, x):
    return activate(w@x > 0)


def activate(arr):
    return (arr * 2) - 1


def count_e(pred, d):
    return d - pred


def count_ek(xk, dk, w):
    return dk - count_y(w, xk)


def avg_ek2(e, x):
    return np.sum(e**2)/x.shape[0]


def next_w(w, lrate, xk, dk):
    return w + 2*lrate*count_ek(xk, dk, w)*xk


# res, weights, dataset = prepare_data(1, 1, -1, -1, 1)
#
# final = create_adaline(dataset, weights, res, 0.25, 0.1)
# print(final)
# print(predictions(set, final))
#
#
# res, weights, dataset = prepare_data(1, 0, -1, -1, 1)
# final_or = create_adaline(set, weights, res, 0.25, 0.1)
# print(final_or)
# print(predictions(set, final_or))
#


def run_tests_adaline(lrates, min_maxes):
    print("tests.csv started")
    iterations = 200
    # os.execl("/usr/bin/rm", "/home/kreisich/PycharmProjects/neural_networks/adaline_results")
    with open('adaline_results', 'w') as csv:
        csv.write("{}|{}|{}|{}|{}\n".format("Function", "Learning rate", "MinMaxes", "epochs", "Final weights"))
    for n in range(2):
        for i in lrates:
            for j in min_maxes:
                with open('adaline_results', 'a') as csv:
                    sum_epochs = 0
                    for z in range(iterations):
                        res, weights, dataset = prepare_data(1, n, j[0], j[1], 1)
                        fw, epochs = create_adaline(dataset, weights, res, i, 0.1)
                        sum_epochs = sum_epochs + epochs
                    sum_epochs = sum_epochs / iterations
                    epochs_result.append(sum_epochs)
                    learning_rates_results.append(i)
                    min_maxes_results.append(j)
                    csv.write("{}|{}|{}|{}|{}\n".format(n, i, j, sum_epochs, fw))


def create_plot(x, y, xlabel, ylabel, title):
    print(x)
    pyplot.scatter(x, y)
    pyplot.xlabel(xlabel)
    pyplot.ylabel(ylabel)
    pyplot.title(title)
    pyplot.show()


def aggregate_data(x, y):
    xd = np.zeros(len(x))
    for i in range(0, len(y), len(x)):
        xd = xd + np.array(y[i: i + len(x)])
    print(xd/2)
    return xd/2

learning_rates = [0.01, 0.05, 0.1, 0.15, 0.2, 0.25]
min_maxes = [(-1, 1)]
# min_maxes = [(-1, 1), (-0.8, 0.8), (-0.5, 0.5), (-0.2, 0.2), (-0.1, 0.1), (-0.05, 0.05)]


epochs_result = []
learning_rates_results = []
min_maxes_results = []

run_tests_adaline(learning_rates, min_maxes)

create_plot(learning_rates, aggregate_data(learning_rates, epochs_result), "Learning rate", "Avg. epochs", "Adaline")

learning_rates = [0.1]
min_maxes = [(-1, 1), (-0.8, 0.8), (-0.5, 0.5), (-0.2, 0.2), (-0.1, 0.1), (-0.05, 0.05)]


epochs_result = []
learning_rates_results = []
min_maxes_results = []

run_tests_adaline(learning_rates, min_maxes)

min_maxes = ["(-1, 1)", "(-0.8, 0.8)", "(-0.5, 0.5)", "(-0.2, 0.2)", "(-0.1, 0.1)", "(-0.05, 0.05)"]
create_plot(min_maxes, aggregate_data(min_maxes, epochs_result), "Min_maxes", "Avg. epochs", "Adaline")

