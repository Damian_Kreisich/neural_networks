import pickle as cPickle
import gzip
import time
from matplotlib import pyplot as plt

# Third-party libraries
import numpy as np


def load_data(file_name):
    f = gzip.open(file_name, 'rb')
    training_data, validation_data, test_data = cPickle.load(f, encoding='latin1')
    f.close()
    return training_data, validation_data, test_data


def soft_max(array):
    return np.exp(array)/np.sum(np.exp(array))


def prepare_random_data(input, layers, min, max, hidden=100, method=0):
    if method == 0:
        wgh = np.random.uniform(low=min, high=max, size=(np.shape(input)[1], layers))
    elif method == 1:
        wgh = np.random.uniform(low=-2.38/np.sqrt(np.shape(input)[1]), high=2.38/np.sqrt(np.shape(input)[1]),
                                size=(np.shape(input)[1], layers))
    elif method == 2:
        wgh = np.random.uniform(low=-hidden**(1/np.shape(input)[1]), high=hidden**(1/np.shape(input)[1]),
                                size=(np.shape(input)[1], layers))
    bias = np.zeros(layers)
    return wgh, bias


def forward_pass_step(input_x, wg, bias, fa):
    result = input_x@wg+bias
    return fa(result), result


def forward_pass(data, weights, biases):
    dataset = np.copy(data)
    activations = [dataset]
    awakens = []
    for i in range(len(weights)-1):
        activated, hidden = forward_pass_step(activations[-1], weights[i], biases[i], get_activation_function('sigmoid'))
        activations.append(activated)
        awakens.append(hidden)

    activated, hidden = forward_pass_step(activations[-1], weights[-1], biases[-1], get_activation_function('sigmoid'))
    activations.append(activated)
    awakens.append(hidden)
    return activations, awakens


def back_propagation(weights, labels, activated, z_res, batch_size):
    dw = []  # dC/dW
    db = []  # dC/dB
    deltas = [None] * len(weights)  # delta = dC/dZ  known as error for each layer

    # insert the last layer error
    deltas[-1] = ((labels - activated[-1]) * (get_derivative_activation_function('sigmoid'))(z_res[-1])).T
    # Perform BackPropagation
    for i in reversed(range(len(deltas) - 1)):
        deltas[i] = (weights[i + 1].dot(deltas[i + 1]).T * (get_derivative_activation_function('sigmoid')(z_res[i]))).T

    db = [d.dot(np.ones((batch_size, 1))) / float(batch_size) for d in deltas]
    dw = [d.dot(activated[i]) / float(batch_size) for i, d in enumerate(deltas)]

    # return the derivitives respect to weight matrix and biases
    return dw, db, deltas


def train(weights, biases, data, labels, validation_data, validation_labels, batch_size=300, epochs=100, lr=0.1, optimizer="adagrad"):
    # update weights and biases based on the output
    print("Warstwa ukryta: {}".format(len(weights[1][0])))
    print("Batch: {}".format(batch_size))
    print("Learning rate: {}".format(lr))

    table = []
    last_change_weights = []
    last_change_biases = []

    m = []
    v = []
    mbias = []
    vbias = []
    for wgh in weights:
        if optimizer == "momentum":
            last_change_weights.append(np.zeros(wgh.shape))
        else:
            last_change_weights.append(np.ones(wgh.shape))
            m.append(np.ones(wgh.shape))
            v.append(np.ones(wgh.shape))
    for bs in biases:
        if optimizer == "momentum":
            last_change_biases.append(np.zeros(bs.shape))
        else:
            last_change_biases.append(np.ones(bs.shape))
            mbias.append(np.ones(bs.shape))
            vbias.append(np.ones(bs.shape))

    print(last_change_biases[0].shape)
    print(biases[0].shape)
    print(last_change_weights[0].shape)
    print(weights[0].shape)
    for e in range(epochs):
        i = 0
        iterations = 0
        while i < len(labels):
            iterations = iterations + 1
            x_batch = data[i:i + batch_size]
            y_batch = labels[i:i + batch_size]
            i = i + batch_size
            activations, results = forward_pass(x_batch, weights, biases)
            dw, db, _ = back_propagation(weights, y_batch, activations, results, len(y_batch))
            if optimizer == "sgd":
                weights = [w + lr * dweight.T for w, dweight in zip(weights, dw)]
                biases = [w + lr * dbias.T for w, dbias in zip(biases, db)]
            elif optimizer == "momentum":
                iteratemom = 0
                for w, dweight in zip(weights, dw):
                    weights[iteratemom] = w + lr * dweight.T + 0.9 * last_change_weights[iteratemom]
                    last_change_weights[iteratemom] = dweight.T
                    iteratemom = iteratemom + 1

                iteratemom = 0
                for w, dbias in zip(biases, db):
                    biases[iteratemom] = w + lr * dbias.T + 0.9 * last_change_biases[iteratemom]
                    last_change_biases[iteratemom] = dbias.T
                    iteratemom = iteratemom + 1

            elif optimizer == "adagrad":
                iteratemom = 0
                for w, dweight in zip(weights, dw):
                    etan = lr / np.sqrt(last_change_weights[iteratemom])
                    weights[iteratemom] = w + etan * dweight.T
                    last_change_weights[iteratemom] = last_change_weights[iteratemom] + dweight.T**2
                    iteratemom = iteratemom + 1

                iteratemom = 0
                for w, dbias in zip(biases, db):
                    etan = lr / np.sqrt(last_change_biases[iteratemom])
                    biases[iteratemom] = w + etan * dbias.T
                    last_change_biases[iteratemom] = last_change_biases[iteratemom] + dbias.T ** 2
                    iteratemom = iteratemom + 1

            elif optimizer == "adadelta":
                iteratemom = 0
                for w, dweight in zip(weights, dw):
                    temp_weights = 0.9 * last_change_weights[iteratemom] + 0.1 * dweight.T ** 2
                    weights[iteratemom] = w + (last_change_weights[iteratemom] / temp_weights) * dweight.T
                    last_change_weights[iteratemom] = temp_weights
                    iteratemom = iteratemom + 1

                iteratemom = 0
                for w, dbias in zip(biases, db):
                    temp_biases = 0.9 * last_change_biases[iteratemom] + 0.1 * dbias.T ** 2
                    biases[iteratemom] = w + (last_change_biases[iteratemom] / temp_biases) * dbias.T
                    last_change_biases[iteratemom] = temp_biases
                    iteratemom = iteratemom + 1
            elif optimizer == "adam":
                iteratemom = 0
                for w, dweight in zip(weights, dw):
                    m[iteratemom] = (0.9 * m[iteratemom] + 0.1 * dweight.T)
                    v[iteratemom] = (0.999 * v[iteratemom] + 0.001 * dweight.T ** 2)
                    weights[iteratemom] = w + (lr / (np.sqrt(v[iteratemom] / (1 - 0.999 ** iterations)) + 10 ** -8)) * \
                                          m[iteratemom] / (1 - 0.9 ** iterations)


                    iteratemom = iteratemom + 1

                iteratemom = 0
                for w, dbias in zip(biases, db):

                    mbias[iteratemom] = (0.9 * mbias[iteratemom] + 0.1 * dbias.T)
                    vbias[iteratemom] = (0.999 * vbias[iteratemom] + 0.001 * dbias.T ** 2)
                    biases[iteratemom] = w + (lr / (np.sqrt(vbias[iteratemom] / (1 - 0.999 ** iterations)) + 10 ** -8)) * mbias[iteratemom] / (1 - 0.9 ** iterations)


                    iteratemom = iteratemom + 1
        single_val = test_validate(weights, biases, validation_data, validation_labels)
        table.append(single_val)
        print("Wynik dla walidującego ciągu {}".format(single_val))
    return weights, table


def create_plot(epochs, values, title):
    fig, ax = plt.subplots(figsize=(5, 3))
    fig.subplots_adjust(bottom=0.15, left=0.2)
    ax.set_title(title)
    x = np.arange(0, epochs)
    y = values
    ax.set_xlabel("Epochs")
    ax.set_ylabel("Values")
    ax.plot(x, y)
    plt.show()


def test_validate(weights, biases, data, labels):
    a, z = forward_pass(data, weights, biases)
    pred = np.argmax(a[-1], axis=1)
    return (np.sum(pred == labels)/len(data))


def get_activation_function(name):
    if name == 'sigmoid':
        def sig(x):
            x = np.clip(x, -500, 500)
            return np.exp(x)/(1+np.exp(x))
        return sig
    elif name == 'linear':
        return lambda x : x
    elif name == 'relu':
        def relu(x):
            y = np.copy(x)
            y[y<0] = 0
            return y
        return relu
    else:
        print('Unknown activation function. linear is used')
        return lambda x: x


def get_derivative_activation_function(name):
    if name == 'sigmoid':
        def sig(x):
            x = np.clip(x, -500, 500)
            return np.exp(x) / (1 + np.exp(x))
        return lambda x :sig(x)*(1-sig(x))
    elif name == 'linear':
        return lambda x: 1
    elif name == 'relu':
        def relu_diff(x):
            y = np.copy(x)
            y[y >= 0] = 1
            y[y < 0] = 0
            return y
        return relu_diff
    else:
        print('Unknown activation function. linear is used')
        return lambda x: 1


def one_hot(labels, outputs):
    zero = np.zeros(shape=(outputs, labels.shape[0]))
    for i, e in enumerate(labels):
        zero[e, i] = 1
    return zero


# training_data, validation_data, test_data = load_data("mnist.pkl.gz")
#
# training_input = training_data[0]
# training_labels = training_data[1]
#
# validation_input = validation_data[0]
# validation_labels = validation_data[1]
#
# test_input = test_data[0]
# test_labels = test_data[1]
#
# hidden_layer = [25, 50, 100, 200, 350, 500]
# batch_size = [10, 100, 300, 1000, 10000]
# hidden_layer = [50]
# batch_size = [50]
# learning_rate = [0.1]
# weights_range = [(-0.25, 0.25)]
# # weights_range = [(-1, 1), (-0.5, 0.5), (-0.25, 0.25), (-0.1, 0.1), (-0.05, 0.05), (-0.01, 0.01)]
# epochs = 25
# reps = 5
# exp = 9

# with open('tests.csv', 'w') as csv:
#     csv.write(
#         "{}|{}|{}|{}|{}|{}|{}|{}\n".format("Learning rate", "MinMaxes", "Batch size", "Hidden layer size", "Epochs",
#                                            "Reps for avg", "Final result", "Time in seconds"))
#
# for hidden in hidden_layer:
#     for size in batch_size:
#         for learn in learning_rate:
#             for rang in weights_range:
#                 timerStart = time.time()
#                 final_table = np.zeros(epochs)
#                 testResults = np.zeros(reps)
#                 for i in range(reps):
#                     weights, biases = prepare_random_data(training_input, hidden, rang[0], rang[1], method=2, hidden=hidden)
#                     first_input = forward_pass_step(training_input, weights, biases, get_activation_function('sigmoid'))
#                     weights1, biases1 = prepare_random_data(first_input[1], 10, rang[0], rang[1], method=2, hidden=hidden)
#
#                     weights = [weights, weights1]
#                     biases = [biases, biases1]
#                     print("Losowane wagi: {}".format(rang))
#
#                     lab = one_hot(training_labels, 10)
#                     weights, table = train(weights, biases, training_input, lab.T, validation_labels=validation_labels,
#                                 validation_data=validation_input, batch_size=size, epochs=epochs, lr=learn, optimizer="adadelta")
#                     final_table = final_table + np.array(table)
#
#                     value = test_validate(weights, biases, test_input, test_labels)
#                     print("Wynik na ciągu testowym {}".format(value))
#
#                     testResults[i] = value
#
#                 timerStop = time.time()
#                 title = ""
#                 if exp == 0:
#                     title = "Range {}".format(rang)
#                 elif exp == 1:
#                     title = "Batch size {}".format(size)
#                 elif exp == 2:
#                     title = "Activation relu"
#                 elif exp == 3:
#                     title = "Hidden layer size {}".format(hidden)
#                 elif exp == 4:
#                     title = "Basic parameters"
#                 elif exp == 5:
#                     title = "Momentum"
#                 elif exp == 6:
#                     title = "AdaGrad"
#                 elif exp == 7:
#                     title = "AdaDelta"
#                 elif exp == 8:
#                     title = "Adam"
#                 elif exp == 9:
#                     title = "Range (-a/sqrt(n), a/sqrt(n))"
#                 elif exp == 10:
#                     title = "Range (-hidden**1/n, hidden**1/n)"
#                 else:
#                     title = "Best score"
#
#                 create_plot(epochs, final_table/reps, title)
#
#                 with open('tests.csv', 'a') as csv:
#                     csv.write(
#                         "{}|{}|{}|{}|{}|{}|{}|{}\n".format(learn, rang, size, hidden, epochs, reps,
#                                                            np.sum(testResults)/reps, timerStop - timerStart))

