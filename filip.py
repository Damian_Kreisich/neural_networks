import numpy as np
import pickle as pkl
import gzip

class Network:

    def __init__(self, num_of_inputs, num_of_neurons_hidden, num_of_classes, learning_rate):
        self.learning_rate = learning_rate
        self.num_of_classes = num_of_classes
        self.W1 = np.random.randn(num_of_inputs, num_of_neurons_hidden)
        self.W2 = np.random.randn(num_of_neurons_hidden, num_of_classes)
        self.b1 = np.random.randn(num_of_neurons_hidden)
        self.b2 = np.random.randn(num_of_classes)
        self.output_2 = None
        self.impulse_2 = None
        self.output_1 = None
        self.impulse_1 = None

    def sigmoid(self, x):
        return 1 / (1 + np.exp(-x))

    def sigmoid_derivatives(self, x):
        sigmoid_value = self.sigmoid(x)
        return sigmoid_value * (1 - sigmoid_value)

    def softmax_derivative(self, predicted, actual, impulse):
        softmax_result = self.softmax(impulse)
        softmax_dv_result = np.zeros(softmax_result.shape)
        # for i in range(softmax_result.shape[0]):
        #     SM = softmax_result[i].reshape((-1, 1))
        #     jac = np.diagflat(softmax_result[i]) - np.dot(SM, SM.T)
        #     val = predicted[i]/softmax_result[i]
        #     cos = jac @ val
        #     softmax_dv_result[i] = cos
        for i in range(softmax_result.shape[0]):
            cos = np.reshape(softmax_result[i], newshape=(softmax_result[i].shape[0], 1))
            e = np.ones(shape=(softmax_result.shape[1], 1))
            I = np.eye(N=softmax_result.shape[1])
            dev = (cos @ e.T) * (I - e @ cos.T)
            softmax_dv_result[i] = dev @ (actual[i] - predicted[i])
        return softmax_dv_result

    def softmax(self, a):
        return np.exp(a - np.max(a)) / np.sum(np.exp(a - np.max(a)))

    def forward_pass_step(self, weights, bias, inputs, activation_func):
        full_impulse = inputs@weights + bias
        return activation_func(full_impulse), full_impulse

    def forward_pass(self, data):
        output_1, impulse_1 = self.forward_pass_step(self.W1, self.b1, data, self.sigmoid)
        output_2, impulse_2 = self.forward_pass_step(self.W2, self.b2, output_1, self.sigmoid)
        self.output_1 = output_1
        self.output_2 = output_2
        self.impulse_1 = impulse_1
        self.impulse_2 = impulse_2
        labels = np.argmax(self.output_2, axis=1)
        return labels, self.output_2

    def back_propagation(self, actual_labels, data):
        actual_labels_one_hot = self.to_one_hot_encoding(actual_labels, self.num_of_classes)
        self.forward_pass(data)
        num = data.shape[0]
        delta2 = self.sigmoid_derivatives(self.impulse_2) * (self.output_2 - actual_labels_one_hot)

        delta1 = self.sigmoid_derivatives(self.impulse_1) * (delta2 @ self.W2.T)
        self.W2 = self.W2 + self.learning_rate * ((self.output_1.T @ delta2) / num)
        self.W1 = self.W1 + self.learning_rate * ((data.T @ delta1) / num)
        self.b2 = self.b2 + self.learning_rate * (delta2.T @ (np.ones(num)) / num)
        self.b1 = self.b1 + self.learning_rate * (delta1.T @ (np.ones(num)) / num)

    def to_one_hot_encoding(self, labels, num_of_classes):
        labels_one_hot = np.zeros(shape=(labels.shape[0], num_of_classes))
        for i in range(len(labels)):
            labels_one_hot[i][labels[i]] = 1
        return labels_one_hot

# def load_data(file_name):
#     f = gzip.open(file_name, 'rb')
#     training_data, validation_data, test_data = pickle.load(f, encoding='latin1')
#     f.close()
#     return training_data, validation_data, test_data

# training_data, validation_data, test_data = load_data("mnist.pkl.gz")
#
# test_data = np.array(training_data[0])
# test_labels = np.array(training_data[1])
#
# valid_data = np.array(validation_data[0])
# valid_labels = np.array(validation_data[1])
#
# neural = Network(784, 100, 10, 0.1)
#
# for i in range(100):
#     for j in range(0, 50000, 50):
#         neural.back_propagation(test_labels[j:j+50], test_data[j:j+50])
#     print(np.sum(neural.forward_pass(valid_data)[0] == valid_labels)/len(valid_labels))


def load_data(pkl_path):
    f = gzip.open(pkl_path, 'rb')

    data = pkl.load(f, encoding='latin1')

    training_data = data[0][0]
    training_labels = data[0][1]
    validation_data = data[1][0]
    validation_labels = data[1][1]
    testing_data = data[2][0]
    testing_labels = data[2][1]

    num_of_classes = np.max(training_labels) + 1

    return (training_data, training_labels), (validation_data, validation_labels), (
    testing_data, testing_labels), num_of_classes


if __name__ == '__main__':
    training, validation, testing, num_of_classes = load_data('mnist.pkl.gz')
    num_of_inputs = training[0].shape[1]
    num_of_neurons_hidden_layer = 30
    n= Network(784, num_of_neurons_hidden_layer, 10, 0.1)
    labels = n.forward_pass(training[0])
    print(np.sum(labels == training[1])/training[0].shape[0])
