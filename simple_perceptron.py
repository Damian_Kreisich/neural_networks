import numpy as np
from matplotlib import pyplot


def prepare_data(mode, function, min, max, bias):
    dataset_array = []
    if (bias == 0):
        dataset_array.append((0, 0))
        dataset_array.append((0, 1))
        dataset_array.append((1, 0))
        dataset_array.append((1, 1))
    elif (bias == 1):
        dataset_array.append((1, 0, 0))
        dataset_array.append((1, 0, 1))
        dataset_array.append((1, 1, 0))
        dataset_array.append((1, 1, 1))

    data_array = []
    if (function == 1):
        data_array.append(0)
        data_array.append(0)
        data_array.append(0)
        data_array.append(1)
    else:
        data_array.append(0)
        data_array.append(1)
        data_array.append(1)
        data_array.append(1)

    dataset = np.array(dataset_array)
    result = np.array(data_array)
    if (mode == 1):
        result = (result * 2) - 1
        dataset = (dataset * 2) - 1

    if bias == 1:
        weights = np.random.uniform(min, max, (1, 3))
    elif bias == 0:
        weights = np.random.uniform(min, max, (1, 2))

    return result, weights, dataset


def count_prediction_error_unipolar(weight, set, desired):
    value = weight@set
    if value < 0.5:
        value = 0
    else:
        value = 1
    return desired-value


def count_prediction_error_bipolar(weight, set, desired):
    value = weight@set
    if value < 0:
        value = -1
    else:
        value = 1
    return desired-value


def create_simple_perceptron(set, wghts, res, lrate, error_function):
    error = 99999
    i = 0
    epoch = 0
    while error != 0:
        for i in range(len(set)):
            wghts = wghts + lrate * error_function(wghts, set[i], res[i]) * set[i]

        epoch = epoch + 1
        error = count_all_errors(error_function, wghts, set, res)

    return wghts, epoch

def count_all_errors(error_function, weights, dataset, result):
    error_sum = 0
    for i in range(len(dataset)):
        error_sum += np.abs(error_function(weights, dataset[i], result[i]))

    return error_sum

def choose_error_function(m):
    if m == 1:
        return count_prediction_error_bipolar
    else:
        return count_prediction_error_unipolar


def run_tests_perceptron(lrates, min_maxes):
    print("tests.csv started")
    iterations = 200
    # os.execl("/usr/bin/rm", "/home/kreisich/PycharmProjects/neural_networks/adaline_results")

    with open('perceptron_results', 'w') as csv:
        csv.write("{}|{}|{}|{}|{}|{}\n".format("Encoding", "Function", "Learning rate", "MinMaxes", "epochs", "Final weights"))
    for m in range(2):
        for n in range(2):
            for i in lrates:
                for j in min_maxes:
                    with open('perceptron_results', 'a') as csv:
                        print("one step")
                        sum_epochs = 0
                        for z in range(iterations):
                            res, weights, dataset = prepare_data(m, n, j[0], j[1], 1)
                            fw, epochs = create_simple_perceptron(dataset, weights, res, i, choose_error_function(m))
                            sum_epochs = sum_epochs + epochs
                        sum_epochs = sum_epochs/iterations
                        epochs_result.append(sum_epochs)
                        csv.write("{}|{}|{}|{}|{}|{}\n".format(m, n, i, j, sum_epochs, fw))


def create_plot(x, y, xlabel, ylabel,title):
    print(x)
    pyplot.scatter(x, y)
    pyplot.xlabel(xlabel)
    pyplot.ylabel(ylabel)
    pyplot.title(title)
    pyplot.show()


def aggregate_data(x, y):
    xd = np.zeros(len(x))
    for i in range(0, len(y), len(x)):
        xd = xd + np.array(y[i: i + len(x)])
    print(xd/2)
    return xd/2


epochs_result = []
learning_rates_results = []
min_maxes_results = []

# learning_rates = [0.01, 0.05, 0.1, 0.15, 0.25]
learning_rates = [0.1]
min_maxes = [(-1, 1), (-0.8, 0.8), (-0.5, 0.5), (-0.2, 0.2), (-0.1, 0.1), (-0.05, 0.05)]

run_tests_perceptron(learning_rates, min_maxes)

min_maxes = ["(-1, 1)", "(-0.8, 0.8)", "(-0.5, 0.5)", "(-0.2, 0.2)", "(-0.1, 0.1)", "(-0.05, 0.05)"]
create_plot(min_maxes, aggregate_data(min_maxes, epochs_result), "Min_maxes", "Avg. epochs", "Simple Perceptron")


epochs_result = []
learning_rates_results = []
min_maxes_results = []

learning_rates = [0.01, 0.05, 0.1, 0.15, 0.2, 0.25]
min_maxes = [(-1, 1)]

run_tests_perceptron(learning_rates, min_maxes)

create_plot(learning_rates, aggregate_data(learning_rates, epochs_result), "Learning rate", "Avg. epochs", "Simple Perceptron")

epochs_result = []
learning_rates_results = []
min_maxes_results = []

learning_rates = [0.1]
min_maxes = [(-1, 1)]

run_tests_perceptron(learning_rates, min_maxes)

functions = ['Unipolar', 'Bipolar']
create_plot(functions, aggregate_data(functions, epochs_result), "Function", "Avg. epochs", "Simple Perceptron")

# res, weights, dataset = prepare_data(0, 0, -1, 1, 0)
# fw, epochs = create_simple_perceptron(dataset, weights, res, learning_rates[0], choose_error_function(0))